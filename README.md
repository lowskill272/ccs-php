<h2>Установка и запуск проекта:</h2>

<h3>Клонируем репозиторий</h3>
<code>git clone https://gitlab.com/lowskill272/ccs-php.git ccs-php</code><br>

<h3>Настраиваем и запускаем проект</h3>
<code>cd ccs-php/my-project<br>
composer install<br>
cp .env.example .env<br>
cd ..<br>
docker-compose up -d
</code><br>

<h3>Генерируем ключ приложения и заполняем базу новостями</h3>
<code>docker-compose exec myapp php artisan key:generate<br>
docker-compose exec myapp php artisan migrate<br>
docker-compose exec myapp php artisan db:seed
</code><br>

Сервер будет запущен на localhost:8000, если порт доступен<br>
Страница с новостями доступна на localhost:8000/news