<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Symfony\Component\DomCrawler\Crawler;

class NewsSeeder extends Seeder
{
    const SOURCE_URL = 'https://www.rbc.ru/';
    const NEWS_ITEM_SELECTOR = 'div.js-news-feed-list a.news-feed__item';
    const IMG_NODE_PATH = 'div.article__main-image > div.article__main-image__wrap > img';
    const NEWS_TEXT_SELECTOR = 'div.article__text p';
    const NEWS_COUNT = 15;


    // TODO: можно сделать обработку ссылок в тексте, но я и так затянул, поэтому решил не усложнять
    /**
     * Зачатки универсальности решения есть, но это пока не готовый вариант, т.к.
     * у различных новостных сайтов присутствуют различные ограничения
     * По-хорошему, нужно писать отдельно функционал для каждого сайта, а общие моменты вынести
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('news')->truncate();

        $html = self::getResource(self::SOURCE_URL);

        $newsBlock = self::getNodeBySelector($html, self::NEWS_ITEM_SELECTOR);
        $news = [];

        foreach ($newsBlock as $item) {
            $link = self::parseLink($item->getAttribute('href'));
            $time = $item->getAttribute('data-modif');

            // Исключаю нестандартный тип статьи, чтобы не затягивать выполнение
            if (strpos($link, 'idei')) {
                continue;
            }

            // Обработка новости из столбца с видео
            if (strpos($link, 'video')) {
                DB::table('news')->insert([
                    'header' => $item->textContent,
                    'preview_text' =>  $item->textContent,
                    'main_image' => '',
                    'full_text' => "
                    <video src='{$link}'>
                        Sorry, your browser doesn't support embedded videos!
                    </video>",
                    'source_created_at' => Carbon::createFromTimestamp($time)->setTimezone('Europe/Moscow')->toDateTimeString()
                ]);

                continue;
            }

            $news[] = [
                'link' => $link,
                'time' => $time
            ];
        }



        $news = array_merge($news, self::getNewsFromAPIRBC($news[array_key_last($news)]['time']));

        foreach ($news as $item) {
            $newsResource = self::getResource($item['link']);

            $text = self::getNodeBySelector(
                $newsResource,
                self::NEWS_TEXT_SELECTOR
            );

            $header = self::getNodeBySelector($newsResource, 'h1')->text();
            $mainImage = self::parseImage($newsResource, self::IMG_NODE_PATH);

            $fullText = '';

            foreach ($text as $tag) {
                $fullText.=$tag->textContent;
            }

            DB::table('news')->insert([
                'header' => $header,
                'preview_text' =>  Str::limit($fullText, 200),
                'main_image' => $mainImage,
                'full_text' => $fullText,
                'source_created_at' => Carbon::createFromTimestamp($item['time'])->setTimezone('Europe/Moscow')->toDateTimeString()
            ]);
        }
    }

    /**
     * Забирает html по заданному url
     * @param $url
     * @return bool|string
     */
    private static function getResource($url): bool|string
    {
        $headers = array(
            'cache-control: max-age=0',
            'upgrade-insecure-requests: 1',
            'user-agent: Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36',
            'sec-fetch-user: ?1',
            'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
            'x-compress: null',
            'sec-fetch-site: none',
            'sec-fetch-mode: navigate',
            'accept-encoding: deflate, br',
            'accept-language: ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7',
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);

        $data = curl_exec($ch);
        curl_close($ch);

        return $data;
    }

    /**
     * Получает объект crawler из html по selector
     * @param $html
     * @param $selector
     * @return Crawler
     */
    private static function getNodeBySelector($html, $selector): Crawler
    {
        $crawler = new Crawler($html);

        return $crawler->filter($selector);
    }

    /**
     * Получает новости, подгружаемые скриптами на rbc
     * @param $lastDate
     * @return array
     */
    private static function getNewsFromAPIRBC($lastDate): array
    {
        $apiNewsLimit = self::NEWS_COUNT - 14 > 10 ? self::NEWS_COUNT - 14 : 10;
        $news = [];

        foreach (json_decode(self::getResource("https://www.rbc.ru/v10/ajax/get-news-feed/project/rbcnews.uploaded/lastDate/$lastDate/limit/$apiNewsLimit"))->items as $item) {
            preg_match('/href="(.*)"/', $item->html, $link);

            $news[] = [
                'link' => $link[1],
                'time' => $item->publish_date_t
            ];
        }

        return array_slice($news, 0, self::NEWS_COUNT - 14);
    }

    /**
     * Парсим относительные ссылки
     * @param $link
     * @return mixed|string
     */
    private static function parseLink($link) {
        if (!isset(parse_url($link)['host'])) {
            $link = 'https://' . parse_url(self::SOURCE_URL)['host'] . $link;
        }

        return $link;
    }

    /**
     * Парсит картинку статьи из node по imgNodePath
     * @param $html
     * @param $imgNodePath
     * @return string
     */
    private static function parseImage($html, $imgNodePath): string
    {
        $imgNode = self::getNodeBySelector($html, $imgNodePath)->first();

        if (!count($imgNode)) {
            return '';
        }

        $src = $imgNode->attr('src');
        $alt = $imgNode->attr('alt');

        return "<img src='$src' alt='$alt'>";
    }
}
