@extends('layouts.app')

@section('content')
    <div class="news-item-container container">
        <h1>{{ $newsItem->header }}</h1>
        <p>{!! $newsItem->main_image !!}</p>
        <p>{{ $newsItem->full_text }}</p>
    </div>
@endsection
