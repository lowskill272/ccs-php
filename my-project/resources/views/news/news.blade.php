@extends('layouts.app')

@section('content')
    <div class="news-container container">
        @foreach ($news as $key => $value)
            <a href="/news/{{ $value->id }}" class="news-item card mt-2">
                <div class="title card-header">
                    <span>{{ $value->header }}</span>
                    <span>{{ $value->source_created_at }}</span>
                </div>
                <div class="preview-text card-body">{{ $value->preview_text }}</div>
            </a>
        @endforeach
    </div>
@endsection
